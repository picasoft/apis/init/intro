# Api/casoft Init Jour 1 : Introduction

Introduction à l'API.

Une chaîne d'intégration permet de construire automatiquement le PDF à partir du fichier .tex présent à la racine.

Après construction (uniquement sur la branche `master`), le document final est disponible [à cette adresse](https://uploads.picasoft.net/api/intro.pdf).

Le chemin d'upload est défini dans le fichier [.gitlab-ci.yml](./gitlab-ci.yml).

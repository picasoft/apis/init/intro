TARGET=main.pdf
LL=latexmk -lualatex
CLEAN=latexmk -C

all: $(TARGET)

.PHONY: clean $(TARGET)

$(TARGET): $(TARGET:%.pdf=%.tex)
	$(LL) $<

clean:
	$(CLEAN)